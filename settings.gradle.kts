rootProject.name = "Spring Boot Integrations"

include("spring-boot-activemq-integration:spring-activemq-consumer",
        "spring-boot-activemq-integration:spring-activemq-consumer-two",
        "spring-boot-activemq-integration:spring-activemq-producer")
include("spring-boot-hibernate-locking")
include("spring-boot-jpa-integration")
include("spring-boot-redis-integration")
include("spring-boot-cahce-integration")
include("spring-boot-swagger-integration")
include("spring-boot-https-integration")
include("spring-boot-custom-validation-integration")
include("spring-boot-pdf-integration")
include("spring-boot-async-integration")