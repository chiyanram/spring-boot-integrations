dependencies {
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("org.thymeleaf:thymeleaf")
    implementation("com.github.jknack:handlebars:4.2.0")
    implementation("net.sf.jtidy:jtidy:r938")
    implementation("org.xhtmlrenderer:flying-saucer-core:9.1.20")
    implementation("org.xhtmlrenderer:flying-saucer-pdf-itext5:9.1.20")
    // https://mvnrepository.com/artifact/org.eclipse.jgit/org.eclipse.jgit
    implementation("org.eclipse.jgit:org.eclipse.jgit:5.7.0.202003110725-r")

}